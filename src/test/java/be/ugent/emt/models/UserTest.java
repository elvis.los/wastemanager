/*
package be.ugent.emt.models;

import be.ugent.emt.business.models.User;
import be.ugent.emt.exceptions.DBException;

import java.sql.SQLException;
import java.util.List;

class UserTest {
    public static void main(String[] args) throws SQLException, DBException {
        // Testen van User-methoden

        // Voorbeeldgebruiker maken
        User user = new User("john_doe", "password123", "John", "Doe", "Employee", "john@example.com", "123");

        // Gebruiker toevoegen aan de database
        user.addUser(user);

        // Alle gebruikers ophalen en afdrukken
        List<User> users = user.getUsers();
        System.out.println("Alle gebruikers:");
        for (User u : users) {
            System.out.println(u.getUserName());
        }

        // Controleren of een gebruiker is geregistreerd
        String testUserName = "john_doe";
        boolean isRegistered = user.isRegistered(testUserName);
        System.out.println(testUserName + " is geregistreerd: " + isRegistered);

        // Controleren of het wachtwoord overeenkomt
        String testPassword = "password123";
        boolean passwordMatches = user.passwordCheck(testUserName, testPassword);
        System.out.println("Wachtwoord komt overeen: " + passwordMatches);

        // Het gebruikerstype van een gebruiker ophalen
        String userType = user.getUserType(testUserName);
        System.out.println("Gebruikerstype van " + testUserName + ": " + userType);

        // Controleren of de gebruiker een werknemer is
        boolean isEmployee = user.checkUserType(testUserName);
        System.out.println(testUserName + " is een werknemer: " + isEmployee);

        // Gebruiker zonder wachtwoord en gebruikerstype ophalen
        User userWithoutPasswordAndUserType = user.getUserWithoutPasswordAndUserType(testUserName);
        System.out.println("Gebruiker zonder wachtwoord en gebruikerstype:");
        System.out.println("UserName: " + userWithoutPasswordAndUserType.getUserName());
        System.out.println("FirstName: " + userWithoutPasswordAndUserType.getFirstName());
        System.out.println("LastName: " + userWithoutPasswordAndUserType.getLastName());
        System.out.println("Email: " + userWithoutPasswordAndUserType.getEmail());
        System.out.println("UserID: " + userWithoutPasswordAndUserType.getUserId());

        // Gebruiker bijwerken
        String[] updateValues = {"UpdatedFirstName", "UpdatedLastName", "UpdatedEmail", "NewPassword"};
        user.updateUser(user, updateValues);

        // Gebruiker na bijwerken ophalen en afdrukken
        User updatedUser = user.getUserWithoutPasswordAndUserType(testUserName);
        System.out.println("Gebruiker na bijwerken:");
        System.out.println("UserName: " + updatedUser.getUserName());
        System.out.println("FirstName: " + updatedUser.getFirstName());
        System.out.println("LastName: " + updatedUser.getLastName());
        System.out.println("Email: " + updatedUser.getEmail());
        System.out.println("UserID: " + updatedUser.getUserId());

        // Gebruiker verwijderen
        user.deleteUser(user);
    }


}*/
