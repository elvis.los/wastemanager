package be.ugent.emt.gui.managerControllers;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;

import java.io.IOException;

public class ManagerOverviewGoalsController {

        @FXML
        private Button add; //verwijst door venster knop add
        @FXML
        Label addLabel;
        @FXML
        private Button delete; //verwijst door venster knop delete
        @FXML
        Label deleteLabel;

        @FXML
        private Button goBackToManagerView;

        @FXML
        Label goBackToManagerViewLabel;



        //vanaf hier aanpassen want klopt niet met add en delete
        public void goToAddButtonOnAction(ActionEvent event) throws IOException {
            try {
                Parent goToAddParent = FXMLLoader.load(getClass().getResource("ManagerAdd.fxml"));
                Scene goToAddScene = new Scene(goToAddParent);
                Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
                window.setTitle("Add");
                window.setResizable(false);
                window.setScene(goToAddScene);
                window.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public void goToChangeBinButtonOnAction(ActionEvent event) throws IOException {
            try {
                Parent goToChangeBinParent = FXMLLoader.load(getClass().getResource("ManagerChangeBin.fxml"));
                Scene goToChangeBinScene = new Scene(goToChangeBinParent);
                Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
                window.setTitle("Change Bin");
                window.setResizable(false);
                window.setScene(goToChangeBinScene);
                window.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public void goToDeleteButtonOnAction(ActionEvent event) throws IOException {
            try {
                Parent goToDeleteParent = FXMLLoader.load(getClass().getResource("ManagerDelete.fxml"));
                Scene goToDeleteScene = new Scene(goToDeleteParent);
                Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
                window.setTitle("Delete");
                window.setResizable(false);
                window.setScene(goToDeleteScene);
                window.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    public void goBackButtonToManagerViewOnAction(java.awt.event.ActionEvent event) throws IOException {
        try {
            Parent goBackToManagerOverviewParent = FXMLLoader.load(getClass().getResource("ManagerView.fxml"));
            Scene goBackToManagerOverviewScene = new Scene(goBackToManagerOverviewParent);
            Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
            window.setTitle("WasteManager Home Screen");
            window.setResizable(false);
            window.setScene(goBackToManagerOverviewScene);
            window.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
