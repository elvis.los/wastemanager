package be.ugent.emt.gui.managerControllers;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.awt.event.ActionEvent;
import java.io.IOException;
import javafx.fxml.FXML;

public class ManagerAddBinController  {
    /*implements Initializable toevoegen
   @FXML
    Label addBinLabel;
    @FXML
    private ChoiceBox<String> wasteTypeChoice;
    @FXML
    private TextField capacityBin;
    @FXML
    Label capacityBinLabel;
    @FXML
    private TextField binIDField;
    @FXML
    Label binIDLabel;
    @FXML
    Label messageWhenPushedOnAddBinButtonLabel;
    @FXML
    private Button goBackToManagerView;
    @FXML
    Label goBackToManagerViewLabel;




    String binID;
    String capacity;
    //WasteType wasteType; //?
    boolean filledIn;

     */
    public void goBackButtonToManagerViewOnAction(ActionEvent event) throws IOException {
        try {
            Parent goBackToManagerOverviewParent = FXMLLoader.load(getClass().getResource("ManagerView.fxml"));
            Scene goBackToManagerOverviewScene = new Scene(goBackToManagerOverviewParent);
            Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
            window.setTitle("WasteManager Home Screen");
            window.setResizable(false);
            window.setScene(goBackToManagerOverviewScene);
            window.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /*

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        wasteType.getItems().addAll("PAPER","PMD","GLASS","REST");
    }

    /*
    //controleert of alle velden zijn ingevuld
    public void filledFields() {
        if (capacity.isBlank() || binID.isBlank() || wasteType) {
            messageWhenPushedOnAddBinButtonLabel.setText("Please fill in all the fields!");
            filledIn = false;
        } else
            messageWhenPushedOnAddBinButtonLabel.setText("");
            filledIn = true;
    }*/

    //voegt nieuwe bin toe
    /*
    public void addBinFinal(ActionEvent event) {
        try {
            capacity = capacityBin.getText();
            binID = binIDField.getText();
            Bin bin = new Bin();
            wasteType = bin.wasteTypeGUI();//???
            filledFields();


            if (filledIn == true) {
                Bin newAddedBin = new Bin(capacity,binID,WasteType.valueOf()); //hoe wasteType toevoegen
                newAddedBin.addBin(newAddedBin); //voegt nieuwe bin toe
                // hierna  vgm doorverwijzen naar overview bins
                Parent binParent = FXMLLoader.load(getClass().getResource("RoomsLandlord.fxml"));
                Scene binScene = new Scene(binParent);
                Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
                window.setTitle("Bins");
               // window.setResizable(false);
               // window.setScene(binScene);
               // window.show();
            } else
                messageWhenPushedOnAddBinButtonLabel.setText("Something went wrong, please try again!");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/



}