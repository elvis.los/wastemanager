package be.ugent.emt.gui.managerControllers;
import be.ugent.emt.business.BinService;
import be.ugent.emt.business.models.Bin;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;

public class ManagerDeleteController {
    @FXML
    Label DeleteCurrentAmountLabel;
    @FXML
    private TextField binIDCurrentAmount;
    @FXML
    private Button DeleteCurrentAmount;
    @FXML
    Label DeleteBinLabel;
    @FXML
    private TextField binIDBin;
    @FXML
    private Button DeleteBin;
    @FXML
    Label changeLabel;
    @FXML
    private Button goBackToManagerView;
    @FXML
    Label goBackToManagerViewLabel;

    BinService binService = new BinService();

    // Wat er gebeurt wanneer de gebruiker op 'deleteCurrentAmount' klikt
    public void deleteCurrentAmountButtonOnAction(ActionEvent event) {
        try {
            // BinID ophalen uit het invoerveld
            String givenBinID = binIDCurrentAmount.getText();

            // Controleren of het invoerveld niet leeg is
            if (givenBinID.isEmpty()) {
                changeLabel.setText("BinID is required.");
                return;
            }

            // Methode aanroepen om de CurrentAmount te verwijderen
            Bin bin = new Bin();
            bin.changeBinByCurrentAmount(0,givenBinID);// 0 betekent hier dat de currentAmount wordt verwijderd

        } catch (Exception e) {
            changeLabel.setText("An error occurred while deleting CurrentAmount.");
        }
    }

    // Wat er gebeurt wanneer de gebruiker op 'deleteBin' klikt
    public void deleteBinButtonOnAction(ActionEvent event) {
        try {
            // BinID ophalen uit het invoerveld
            String givenBinID = binIDBin.getText();

            // Controleren of het invoerveld niet leeg is
            if (givenBinID.isEmpty()) {
                changeLabel.setText("BinID is required.");
                return;
            }

            // Methode aanroepen om de Bin te verwijderen
            binService.deleteBin(givenBinID);

        } catch (Exception e) {
            changeLabel.setText("An error occurred while deleting Bin.");
        }
    }

    //wat er gebeurt wanneer de gebruiker op 'goBack klikt
    public void goBackButtonToManagerViewOnAction(java.awt.event.ActionEvent event) throws IOException {
        try {
            Parent goBackToManagerOverviewParent = FXMLLoader.load(getClass().getResource("ManagerView.fxml"));
            Scene goBackToManagerOverviewScene = new Scene(goBackToManagerOverviewParent);
            Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
            window.setTitle("WasteManager Home Screen");
            window.setResizable(false);
            window.setScene(goBackToManagerOverviewScene);
            window.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
