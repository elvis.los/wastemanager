/*
package be.ugent.emt.gui.managerControllers;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import be.ugent.emt.models.User;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class ManagerAddEmployeeController implements Initializable {
    @FXML
    private TableView<User> Table;

    @FXML
    private TableColumn<User, String> Username;
    @FXML
    private TableColumn<User,String> FirstName;
    @FXML
    private TableColumn<User,String> LastName;
    @FXML
    private TableColumn<User,String> UserType;
    @FXML
    private TableColumn<User,String> Email;

    @FXML
    private Button addEmployeeButton;
    @FXML
    private Button goBack;
    @FXML
    private Label messageWhenClickedOnAddEmployeeButton;
    @FXML
    private Button deleteEmployee;
    @FXML
    private TextField userNameField;

    User user = new User();
    ObservableList<User> observableList = FXCollections.observableList(user.getUsers()); //hoe enkel employees miss methode getAllEmployees
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        Username.setCellValueFactory(new PropertyValueFactory<User,String>("userName"));
        FirstName.setCellValueFactory(new PropertyValueFactory<User,String>("firstName"));
        LastName.setCellValueFactory(new PropertyValueFactory<User,String>("lastName"));
        UserType.setCellValueFactory(new PropertyValueFactory<User,String>("userType"));
        Email.setCellValueFactory(new PropertyValueFactory<User,String>("email"));
        Table.setItems(observableList);
    }

    public boolean insertWasteManagerName(){
        if(user.checkUserType(userNameField.getText())) {
            messageWhenClickedOnAddEmployeeButton.setText("Insert an employee username!");
            //Als de gebruiker een "WasteManager" is wordt een foutbericht weergegeven
            return false;
        }
        else {
            messageWhenClickedOnAddEmployeeButton.setText("");
            return true;
        }
    }

    //wat gebeurt als op add employee drukt
    public void addEmployee(ActionEvent event) {
        try {

            if(insertWasteManagerName()) {
                observableList.add(user.getUserWithoutPasswordAndUserType(userNameField.getText()));
                Table.setItems(observableList);
            }
            else
                messageWhenClickedOnAddEmployeeButton.setText("Insert an employee username!");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //wat er gebeurt als de gebruiker op 'Delete employee' drukt
    public void deleteEmployee(ActionEvent event) {
        try {
            int selectedItem = Table.getSelectionModel().getSelectedIndex();
            if (selectedItem >= 0) {
                Table.getItems().remove(selectedItem);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    //wat er gebeurt wanneer de gebruiker op 'goBack klikt
    public void goBackButtonToManagerViewOnAction(java.awt.event.ActionEvent event) throws IOException {
        try {
            Parent goBackToManagerOverviewParent = FXMLLoader.load(getClass().getResource("ManagerView.fxml"));
            Scene goBackToManagerOverviewScene = new Scene(goBackToManagerOverviewParent);
            Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
            window.setTitle("WasteManager Home Screen");
            window.setResizable(false);
            window.setScene(goBackToManagerOverviewScene);
            window.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
*/
