/*
package be.ugent.emt.gui.managerControllers;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import be.ugent.emt.models.User;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class ManagerOverviewEmployeesController implements Initializable {
    @FXML
    private TableView<User> table;
    @FXML
    private TableColumn<User, String> userName;
    @FXML
    private TableColumn<User, String> passWord;
    @FXML
    private TableColumn<User, String> firstName;
    @FXML
    private TableColumn<User, String> lastName;
    @FXML
    private TableColumn<User, String> email;
    @FXML
    private TableColumn<User, String> userID;
    @FXML
    private Button addEmployees;
    @FXML
    Label addEmployeesLabel;
    @FXML
    private Button goBackToManagerView;
    @FXML
    Label goBackToManagerViewLabel;
    User user = new User();

    ObservableList<User> list = FXCollections.observableArrayList(user.getUsers());//is dit voldoende

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        userName.setCellValueFactory(new PropertyValueFactory<User,String>("userName"));
        passWord.setCellValueFactory(new PropertyValueFactory<User,String>("passWord"));
        firstName.setCellValueFactory(new PropertyValueFactory<User,String>("firstName"));
        lastName.setCellValueFactory(new PropertyValueFactory<User,String>("lastName"));
        email.setCellValueFactory(new PropertyValueFactory<User,String>("email"));
        userID.setCellValueFactory(new PropertyValueFactory<User,String>("userID"));
        table.setItems(list);
    }

    //wat er gebeurt wanneer de gebruiker op 'add employees' klikt
    public void goToAddEmployeesButtonOnAction(ActionEvent event) throws IOException {
        try {
            Parent goToAddEmployeesParent = FXMLLoader.load(getClass().getResource("ManagerAddEmployees.fxml"));
            Scene goToAddEmployeesScene = new Scene(goToAddEmployeesParent);
            Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
            window.setTitle("Add/Delete Employees");
            window.setResizable(false);
            window.setScene(goToAddEmployeesScene);
            window.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //wat er gebeurt wanneer de gebruiker op 'goBack klikt
    public void goBackButtonToManagerViewOnAction(java.awt.event.ActionEvent event) throws IOException {
        try {
            Parent goBackToManagerOverviewParent = FXMLLoader.load(getClass().getResource("ManagerView.fxml"));
            Scene goBackToManagerOverviewScene = new Scene(goBackToManagerOverviewParent);
            Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
            window.setTitle("WasteManager Home Screen");
            window.setResizable(false);
            window.setScene(goBackToManagerOverviewScene);
            window.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
*/
