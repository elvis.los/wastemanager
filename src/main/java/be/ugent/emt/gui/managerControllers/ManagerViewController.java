package be.ugent.emt.gui.managerControllers;

import be.ugent.emt.exceptions.DBException;
import be.ugent.emt.gui.navigationUtil.NavigationUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;

import java.io.IOException;

import static javafx.application.Application.launch;

public class ManagerViewController {


    @FXML
    Label usernameWasteManagerLabel;
    @FXML
    private Button overviewGoals;
    @FXML
    Label overviewGoalsLabel;
    @FXML
    private Button overviewBins;
    @FXML
    Label overviewBinsLabel;
    @FXML
    private Button overviewRequests;
    @FXML
    Label overviewRequestsLabel;
    private Button requestActions;
    Label requestActionsLabel;
    @FXML
    private Button overviewEmployees;
    @FXML
    Label overviewEmployeesLabel;
    @FXML
    private Button logout;


    //laat onderstaande tekst verschijnen
    public void displayName(String username) {
        usernameWasteManagerLabel.setText("Welcome, " + username + "!");
    }

    //wat er gebeurt wanneer de gebruiker op 'Overview Bins' klikt
    public void goToOverviewBinsButtonOnAction(ActionEvent event) throws IOException {
        try {
            Parent goToOverviewBinsParent = FXMLLoader.load(getClass().getResource("ManagerOverviewBins.fxml"));
            Scene goToOverviewBinsScene = new Scene(goToOverviewBinsParent);
            Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
            window.setTitle("Overview Bins");
            window.setResizable(false);
            window.setScene(goToOverviewBinsScene);
            window.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //wat er gebeurt wanneer de gebruiker op 'Overview Goals' klikt
    public void goToOverviewGoalsButtonOnAction(ActionEvent event) throws IOException {
        try {
            Parent goToOverviewGoalsParent = FXMLLoader.load(getClass().getResource("ManagerOverviewGoals.fxml"));
            Scene goToOverviewGoalsScene = new Scene(goToOverviewGoalsParent);
            Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
            window.setTitle("Overview Goals");
            window.setResizable(false);
            window.setScene(goToOverviewGoalsScene);
            window.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //wat er gebeurt wanneer de gebruiker op 'Overview Requests' klikt
    public void goToOverviewRequestsButtonOnAction(ActionEvent event) throws IOException {
        try {
            Parent goToOverviewRequestsParent = FXMLLoader.load(getClass().getResource("ManagerOverviewRequests.fxml"));
            Scene goToOverviewRequestsScene = new Scene(goToOverviewRequestsParent);
            Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
            window.setTitle("Overview Requests");
            window.setResizable(false);
            window.setScene(goToOverviewRequestsScene);
            window.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void goToRequestActionsButtonOnAction(ActionEvent event) throws IOException {
        try {
            Parent goToRequestActionsParent = FXMLLoader.load(getClass().getResource("ManagerRequestActions.fxml"));
            Scene goToRequestActionsScene = new Scene(goToRequestActionsParent);
            Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
            window.setTitle("Requests Actions");
            window.setResizable(false);
            window.setScene(goToRequestActionsScene);
            window.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //wat er gebeurt wanneer de gebruiker op 'Overview Employees' klikt
    public void goToOverviewEmployeesButtonOnAction(ActionEvent event) throws IOException {
        try {
            Parent goToOverviewEmployeesParent = FXMLLoader.load(getClass().getResource("ManagerOverviewEmployees.fxml"));
            Scene goToOverviewEmployeesScene = new Scene(goToOverviewEmployeesParent);
            Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
            window.setTitle("Overview Employees");
            window.setResizable(false);
            window.setScene(goToOverviewEmployeesScene);
            window.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void goBackToStart(ActionEvent event) throws IOException, DBException {
        NavigationUtils.goBackToStart();
    }
}

