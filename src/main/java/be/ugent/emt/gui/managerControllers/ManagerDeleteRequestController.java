package be.ugent.emt.gui.managerControllers;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.IOException;

public class ManagerDeleteRequestController {

    @FXML
    private Button goBackToManagerView;
    @FXML
    Label goBackToManagerViewLabel;

    public void goBackButtonToManagerViewOnAction(ActionEvent event) throws IOException {
        try {
            Parent goBackToManagerOverviewParent = FXMLLoader.load(getClass().getResource("ManagerView.fxml"));
            Scene goBackToManagerOverviewScene = new Scene(goBackToManagerOverviewParent);
            Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
            window.setTitle("WasteManager Home Screen");
            window.setResizable(false);
            window.setScene(goBackToManagerOverviewScene);
            window.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
