package be.ugent.emt.gui.managerControllers;
import be.ugent.emt.business.BinService;
import be.ugent.emt.business.models.Bin;
import be.ugent.emt.exceptions.DBException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;

public class ManagerChangeBinController {
    @FXML
    private TextField binID;
    @FXML
    Label binIDLabel;
    @FXML
    private TextField newCurrentAmount;
    @FXML
    Label newCurrentAmountLabel;
    @FXML
    private Button submit;
    @FXML
    Label changeLabel;
    @FXML
    private Button goBackToManagerView;
    @FXML
    Label goBackToManagerViewLabel;

    BinService binService = new BinService();

    // Wat er gebeurt wanneer de gebruiker op 'change' klikt
    public void changeButtonOnAction(ActionEvent event) {
        try {
            // BinID en nieuwe CurrentAmount ophalen uit velden
            String givenBinID = binID.getText();
            String newCurrentAmountText = newCurrentAmount.getText();

            // Controleren of de invoervelden niet leeg zijn
            if (givenBinID.isEmpty() || newCurrentAmountText.isEmpty()) {
                changeLabel.setText("Both fields are required.");
                return;
            }

            // Nieuwe CurrentAmount converteren naar een integer
            int newCurrentAmountInt = Integer.parseInt(newCurrentAmountText);

            // Methode aanroepen om de bin bij te werken
            binService.updateCurrentBinAmount(givenBinID,newCurrentAmountInt);

        } catch (NumberFormatException e) {
            changeLabel.setText("Please enter a valid number for CurrentAmount.");
        } catch (DBException e) {
            throw new RuntimeException(e);
        }
    }



    //wat er gebeurt wanneer de gebruiker op 'goBack' klikt
    public void goBackButtonToManagerViewOnAction(java.awt.event.ActionEvent event) throws IOException {
        try {
            Parent goBackToManagerOverviewParent = FXMLLoader.load(getClass().getResource("ManagerView.fxml"));
            Scene goBackToManagerOverviewScene = new Scene(goBackToManagerOverviewParent);
            Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
            window.setTitle("WasteManager Home Screen");
            window.setResizable(false);
            window.setScene(goBackToManagerOverviewScene);
            window.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
