package be.ugent.emt.gui.employeeControllers;

import be.ugent.emt.business.GoalService;
import be.ugent.emt.business.models.Goal;
import be.ugent.emt.exceptions.DBException;
import be.ugent.emt.gui.navigationUtil.NavigationUtils;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import javafx.event.ActionEvent;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class EmployeeOverviewGoalsController {

    @FXML
    private ListView<String> pmdListView;
    @FXML
    private ListView<String> restListView;
    @FXML
    private ListView<String> paperListView;
    @FXML
    private ListView<String> glassListView;

    private final GoalService goalService = new GoalService();
    private static final List<String> WASTE_TYPES = Arrays.asList("PMD", "REST", "PAPER", "GLASS");

    public void initialize() {
        try {
            updateGoalListViews();
        } catch (DBException e) {
            // Handle DBException appropriately, possibly logging the error and notifying the user
        }
    }

    private void updateGoalListViews() throws DBException {
        for (String wasteType : WASTE_TYPES) {
            int currentAmount = goalService.getCurrentAmountOfRequestsPerWasteType(wasteType);
            int goalAmount = goalService.getGoalAmountOfRequestsPerWasteType(wasteType);
            String status = goalService.getGoalStatusPerWasteType(wasteType);

            String currentAmountStatus = "Huidig aantal requests: " + currentAmount;
            String goalAmountStatus = "Ideale goal aantal requests: " + goalAmount;

            updateListView(wasteType, currentAmountStatus, goalAmountStatus, status);
        }
    }

    private void updateListView(String wasteType, String currentAmountStatus, String goalAmountStatus, String status) {
        ListView<String> targetListView = getListViewByWasteType(wasteType);
        if (targetListView != null) {
            targetListView.getItems().clear();
            targetListView.getItems().addAll(currentAmountStatus, goalAmountStatus, status);
        }
    }

    private ListView<String> getListViewByWasteType(String wasteType) {
        return switch (wasteType) {
            case "PMD" -> pmdListView;
            case "REST" -> restListView;
            case "PAPER" -> paperListView;
            case "GLASS" -> glassListView;
            default -> null; // handle unexpected waste type
        };
    }

    public void goBackToStart(ActionEvent event) throws IOException {
        try {
            NavigationUtils.goBackToEmployeeView();
        } catch (DBException e) {
            throw new IOException("Error while going back to employee view", e);
        }
    }

}
