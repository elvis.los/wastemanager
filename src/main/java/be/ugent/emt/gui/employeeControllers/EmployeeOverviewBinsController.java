package be.ugent.emt.gui.employeeControllers;

import be.ugent.emt.business.BinService;
import be.ugent.emt.business.models.Bin;
import be.ugent.emt.exceptions.DBException;
import be.ugent.emt.gui.navigationUtil.NavigationUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;

import java.io.IOException;
import java.util.List;

public class EmployeeOverviewBinsController {

    @FXML
    private ListView<Bin> pmdListView;

    @FXML
    private ListView<Bin> restListView;

    @FXML
    private ListView<Bin> paperListView;

    @FXML
    private ListView<Bin> glassListView;

    BinService binService = new BinService();

    public void initialize() throws DBException {
        /*
        Hier kun je de initialisatie van je scherm toevoegen als die geopend wordt, dus:
        - De lijsten vullen met de goals van de gebruiker per wasteType
        */
        addBinsToListViews();
    }

    private void addBinsToListViews() throws DBException {
        // Krijg een lijst terug met daarin 4 lijsten, 1 per wasteType
        List<List<Bin>> binsPerWasteType = binService.getAllBinsByWasteType();

        List<Bin> pmdBins = binsPerWasteType.get(0);
        List<Bin> restBins = binsPerWasteType.get(1);
        List<Bin> paperBins = binsPerWasteType.get(2);
        List<Bin> glassBins = binsPerWasteType.get(3);

        // Voeg de bins toe aan de lijsten
        pmdListView.getItems().addAll(pmdBins);
        restListView.getItems().addAll(restBins);
        paperListView.getItems().addAll(paperBins);
        glassListView.getItems().addAll(glassBins);

    }

    public void goBackToStart(ActionEvent event) throws IOException, DBException {
        NavigationUtils.goBackToEmployeeView();

    }
}
