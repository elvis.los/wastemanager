package be.ugent.emt.gui.employeeControllers;

import be.ugent.emt.exceptions.DBException;
import be.ugent.emt.gui.navigationUtil.NavigationUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;

import java.io.IOException;


public class EmployeeViewController {

    @FXML
    private Label usernameEmployeeLabel;
    private Button overviewGoals;
    @FXML
    Label overviewGoalsLabel;
    @FXML
    private Button overviewBins;
    @FXML
    Label overviewBinsLabel;
    @FXML
    private Button logout;


    public void displayName(String username) {
        usernameEmployeeLabel.setText("Welcome, " + username + "!");
    }

    // Als de gebruiker op  'Bins Overview' klikt, open het scherm met de bins
    public void goToBinsOverview(ActionEvent event) throws DBException {
        System.out.println("Bins Overview button pushed!");
        try {
            FXMLLoader loaderBinsOverview = new FXMLLoader(getClass().getResource("/EmployeeOverviewBins.fxml"));
            Parent root = loaderBinsOverview.load();

            Stage window = new Stage();
            Scene loadBinsOverviewScene = new Scene(root);
            window.setResizable(false);
            window.setScene(loadBinsOverviewScene);
            window.show();
        } catch (IOException e) {
            throw new DBException("Error while loading the EmployeeOverviewBins.fxml file", e);
        }
    }

    // Als de gebruiker op  'Goals Overview' klikt, open het scherm met de goals
    public void goToGoalsOverview(ActionEvent event) throws DBException {
        System.out.println("Goals Overview button pushed!");
        try {
            FXMLLoader loaderBinsOverview = new FXMLLoader(getClass().getResource("/EmployeeOverviewGoals.fxml"));
            Parent root = loaderBinsOverview.load();

            Stage window = new Stage();
            Scene loadBinsOverviewScene = new Scene(root);
            window.setResizable(false);
            window.setScene(loadBinsOverviewScene);
            window.show();
        } catch (IOException e) {
            throw new DBException("Error while loading the BinsOverview.fxml file", e);
        }

    }


    public void goBackToStart(ActionEvent event) throws IOException, DBException {
        System.out.println("Back to start button pushed!");
        // Als de gebruiker op 'Back' klikt, ga terug naar het startscherm (code wordt uitgevoerd in NavigationUtils -> anders moet je deze methode teveel opnieuw schrijven)
        NavigationUtils.goBackToStart();
    }


}
