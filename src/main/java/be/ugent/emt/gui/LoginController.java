package be.ugent.emt.gui;

import be.ugent.emt.business.AuthenticationService;
import be.ugent.emt.business.UserService;
import be.ugent.emt.exceptions.DBException;
import be.ugent.emt.gui.employeeControllers.EmployeeViewController;
import be.ugent.emt.gui.managerControllers.ManagerViewController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.io.IOException;

import static javafx.application.Application.launch;

public class LoginController {
    @FXML
    private TextField usernameField;
    @FXML
    private PasswordField passwordField;
    @FXML
    private Label messageLabel;
    @FXML
    private Button loginButton;
    @FXML
    private Button registerButton;

    // Dit zijn u service klassen:
    // Deze klasse checked doet alles met de login en de registratie
    private final AuthenticationService authenticationService = new AuthenticationService();
    private final UserService userService = new UserService();


    public void initialize() {
        // Hier kun je de initialisatie van je scherm toevoegen als die geopend wordt
        // Bijvoorbeeld: Stel standaardwaarden in voor tekstvelden
        usernameField.setText("");
        passwordField.setText("");
    }

    public void loginButtonPushed(ActionEvent event) throws DBException {
        System.out.println("Login button pushed!");
        String username = usernameField.getText();
        String password = passwordField.getText();

        System.out.println(username);
        System.out.println(password);

        // Check if the fields are filled in
        if (username.isBlank() || password.isBlank()) {
            messageLabel.setText("Please fill in all the fields!");
        }
        // Check if the user exists
        if (!authenticationService.isCorrectUsernameAndPwd(username, password)) {
            messageLabel.setText("Unknown username or password, please sign up!");
            return;
        }
        System.out.println(userService.isEmployee(username));
        if (userService.isEmployee(username)) {
            System.out.println("User is an employee");
            // If the user is an employee, go to the employee view
            loadEmployeeView(event, username); // (-> THIS WORKS)
            return;

        } else if (userService.isWasteManager(username)) {
            System.out.println("User is a waste manager");
            // If the user is a waste manager, go to the waste manager view
            loadWasteManagerView(event, username); // (-> THIS WORKS)
            return;

        }
        System.out.println("User is not an employee or a waste manager");

    }

    private void loadWasteManagerView(ActionEvent event, String username) throws DBException {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/ManagerView.fxml"));
            Parent root = loader.load();

            ManagerViewController managerViewController = loader.getController();
            managerViewController.displayName(username);

            Stage window = new Stage();
            Scene loginManagerScene = new Scene(root);
            window.setTitle("Wastemanager Home Screen");
            window.setResizable(false);
            window.setScene(loginManagerScene);
            window.show();
        } catch (IOException e) {
            throw new DBException("Error while loading the WasteManagerView.fxml file", e);
        }
    }

    private void loadEmployeeView(ActionEvent event, String username) throws DBException {
        try {
            FXMLLoader loaderEmployee = new FXMLLoader(getClass().getResource("/EmployeeView.fxml"));
            Parent root = loaderEmployee.load();

            EmployeeViewController employeeViewController = loaderEmployee.getController();
            employeeViewController.displayName(username);


            Stage window = new Stage();
            Scene loginEmployeeScene = new Scene(root);
            window.setTitle("Employee Home Screen");
            window.setResizable(false);
            window.setScene(loginEmployeeScene);
            window.show();
        } catch (IOException e) {
            throw new DBException("Error while loading the EmployeeView.fxml file", e);
        }
    }


    public void goToRegistrationButtonPushed(ActionEvent actionEvent) throws DBException {
        // Go to the registration view
        try {
            Parent goToRegistrationParent = FXMLLoader.load(getClass().getResource("/RegisterView.fxml"));
            Scene goToRegistrationScene = new Scene(goToRegistrationParent);
            Stage window = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
            window.setTitle("Create an account");
            window.setResizable(false);
            window.setScene(goToRegistrationScene);
            window.show();
        } catch (IOException e) {
            throw new DBException("Error while loading the RegisterView.fxml file", e);
        }
    }
}






