package be.ugent.emt.gui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class App extends Application {

    public static void main(String[] args) {
        //testen
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {

        try {
            // Attempt to load FXML file the getResource() method goes to the resources folder and finds the file
            Parent fxmlContent = FXMLLoader.load(App.class.getResource("/LoginView.fxml"));
            Scene scene = new Scene(fxmlContent);
            stage.setScene(scene);
            stage.show();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("lalala");
            throw new Exception(e);
        }

    }
}
