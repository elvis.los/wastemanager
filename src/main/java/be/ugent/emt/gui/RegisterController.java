package be.ugent.emt.gui;

import be.ugent.emt.business.UserService;
import be.ugent.emt.exceptions.DBException;
import be.ugent.emt.gui.employeeControllers.EmployeeViewController;
import be.ugent.emt.gui.managerControllers.ManagerViewController;
import be.ugent.emt.gui.navigationUtil.NavigationUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.SQLException;

public class RegisterController {

    @FXML
    private TextField usernameTextField;

    @FXML
    private TextField emailTextField;

    @FXML
    private CheckBox employeeCheckbox;

    @FXML
    private CheckBox wasteManagerCheckbox;

    @FXML
    private TextField passwordTextField;

    @FXML
    private Label errorLabel;

    @FXML
    private TextField confirmPasswordTextField;

    // Dit is een instantie van de UserService, zodat we de methodes kunnen gebruiken van deze klasse
    private final UserService userService = new UserService();


    // Als de gebruiker op 'Sign Up' klikt, check of alles klopt en maak een nieuwe gebruiker aan
    public void createAccount(ActionEvent event) throws DBException {
        String email = emailTextField.getText();
        String username = usernameTextField.getText();
        String password = passwordTextField.getText();
        String confirmPassword = confirmPasswordTextField.getText();
        boolean employeeBox = employeeCheckbox.isSelected();
        boolean wasteManagerBox = wasteManagerCheckbox.isSelected();

        try {
            if (validateInputs(username, email, password, confirmPassword, employeeBox, wasteManagerBox)) {
                int userID = userService.createUserID();

                // Als employeeBox is aangevinkt, maak een nieuwe employee aan, anders maak een nieuwe waste manager aan
                String personType = employeeBox ? "Employee" : "Waste Manager";
                userService.addUser(username, password, "", "", personType, email, userID);

                if (employeeBox) {
                    userService.addEmployee(userID);
                    loadEmployeeView(event, username);
                } else {
                    userService.addManager(userID);
                    loadWasteManagerView(event, username);
                }
            }
        } catch (DBException | SQLException e) {
            errorLabel.setText("Er is een fout opgetreden. Probeer het opnieuw.");
            throw new DBException("Error while creating a new user or opening view", e);

        }
    }

    private boolean validateInputs(String username, String email, String password, String confirmPassword, boolean employeeBox, boolean wasteManagerBox) throws DBException, SQLException {
        StringBuilder errorMessage = new StringBuilder();

        if (!filledFields(username, email, password, confirmPassword)) {
            System.out.println(!filledFields(username, email, password, confirmPassword));
            errorMessage.append("Vul alle velden in.\n");
        }

        if (!onlyOneCheckboxIsChecked(employeeBox, wasteManagerBox)) {
            System.out.println(!onlyOneCheckboxIsChecked(employeeBox, wasteManagerBox));
            errorMessage.append("Selecteer maar één of ten minste één vakje.\n");
        }

        if (!passwordEqualsConfirmedPwd(password, confirmPassword)) {
            System.out.println(!passwordEqualsConfirmedPwd(password, confirmPassword));
            errorMessage.append("Passwoord komt niet overeen, gelieve opnieuw te proberen!\n");
        }

        if (usernameIsTaken(username)) {
            System.out.println(usernameIsTaken(username));
            errorMessage.append("Gebruikersnaam al in gebruik.\n");
        }

        if (!errorMessage.isEmpty()) {
            System.out.println(errorMessage);
            errorLabel.setText(errorMessage.toString());
            return false;
        }
        System.out.println("All inputs are valid");
        return true;
    }

    public boolean filledFields(String username, String email, String password, String confirmPassword) {
        return !username.isBlank() && !email.isBlank() && !password.isBlank() && !confirmPassword.isBlank();
    }


    // Controleert of er maar één checkbox is aangeduid
    public boolean onlyOneCheckboxIsChecked(boolean employeeBox, boolean wasteManagerBox) {
        return employeeBox && !wasteManagerBox || !employeeBox && wasteManagerBox;

    }

    //controleert of password gelijk is aan confirmpassword
    public boolean passwordEqualsConfirmedPwd(String password, String confirmPassword) {
        return password.equals(confirmPassword);
    }

    // controleert of de username nog niet in gebruik is
    public boolean usernameIsTaken(String username) throws DBException, SQLException {
        if (username.isBlank()) {
            return false;
        }
        return userService.userExists(username);
    }

    private void loadEmployeeView(ActionEvent event, String username) throws DBException {
        try {
            FXMLLoader loaderEmployee = new FXMLLoader(getClass().getResource("/EmployeeView.fxml"));
            Parent root = loaderEmployee.load();

            EmployeeViewController employeeViewController = loaderEmployee.getController();
            employeeViewController.displayName(username);


            Stage window = new Stage();
            Scene loginEmployeeScene = new Scene(root);
            window.setTitle("Employee Home Screen");
            window.setResizable(false);
            window.setScene(loginEmployeeScene);
            window.show();
        } catch (IOException e) {
            throw new DBException("Error while loading the EmployeeView.fxml file", e);
        }
    }

    private void loadWasteManagerView(ActionEvent event, String username) throws DBException {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/ManagerView.fxml"));
            Parent root = loader.load();

            ManagerViewController managerViewController = loader.getController();
            managerViewController.displayName(username);

            Stage window = new Stage();
            Scene loginManagerScene = new Scene(root);
            window.setTitle("Wastemanager Home Screen");
            window.setResizable(false);
            window.setScene(loginManagerScene);
            window.show();
        } catch (IOException e) {
            throw new DBException("Error while loading the WasteManagerView.fxml file", e);
        }
    }


    // Als de gebruiker op 'Back' klikt, ga terug naar het startscherm
    public void goBackToStart(ActionEvent event) throws IOException, DBException {
        NavigationUtils.goBackToStart();
    }

}


