/**package be.ugent.emt.gui;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.stage.Stage;
import javafx.application.Application;
import models.be.ugent.CollectionRequest;


import java.awt.event.ActionEvent;
import java.io.IOException;
import java.util.Collections;

public class RequestActions{
    public static void display(){
        Stage window = new Stage();
        window.setTitle("RequestActions");
    }
    private Button addRequestButton;
    private Button deleteRequestButton;
    private Button goBackButton;
    private TableView<CollectionRequest> Table;
    Parent root;
    public void goBack (ActionEvent event) throws IOException{
        try{
            FXMLLoader loader = new FXMLLoader(getClass().getResource("WasteManagerView.fxml"));
            root = loader.load();
            ManagerViewController managerViewController = loader.getController();
            managerViewController.displayName(LoginController.username);
            Scene backToManagerViewScene = new Scene(root);
            Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
            window.setTitle("ManagerOverview");
            window.setResizable(false);
            window.setScene(backToManagerViewScene);
            window.show();
        }
        catch (IOException e){
            e.printStackTrace();
        }
    }
    public void deleteRequest(ActionEvent event){
        int selectedItem=Table.getSelectionModel().getSelectedIndex();
        CollectionRequest request=new CollectionRequest();
        request.deleteRequest(request.getCollection);

    }
    public void goToAddRequest(ActionEvent event) throws IOException {
        try {
            Parent goToAddRequestParent = FXMLLoader.load(getClass().getResource("AddRequest.fxml"));
            Scene goToAddRequestScene = new Scene(goToAddRequestParent);
            Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
            window.setTitle("Add measurement");
            window.setResizable(false);
            window.setScene(goToAddRequestScene);
            window.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
//*/
