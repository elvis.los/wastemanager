package be.ugent.emt.gui.navigationUtil;

import be.ugent.emt.exceptions.DBException;
import be.ugent.emt.gui.employeeControllers.EmployeeViewController;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class NavigationUtils {
    public static void goBackToStart() throws IOException, DBException {
        try {
            FXMLLoader loader = new FXMLLoader(NavigationUtils.class.getResource("/LoginView.fxml"));
            Parent root = loader.load();

            Stage window = new Stage();
            Scene loginManagerScene = new Scene(root);

            window.setResizable(false);
            window.setScene(loginManagerScene);
            window.show();
        } catch (IOException e) {
            throw new DBException("Error while loading the LoginView.fxml file", e);
        }
    }

    public static void goBackToEmployeeView() throws IOException, DBException {
        try {
            FXMLLoader loaderEmployee = new FXMLLoader(NavigationUtils.class.getResource("/EmployeeView.fxml"));
            Parent root = loaderEmployee.load();

            Stage window = new Stage();
            Scene loginEmployeeScene = new Scene(root);
            window.setTitle("Employee Home Screen");
            window.setResizable(false);
            window.setScene(loginEmployeeScene);
            window.show();
        } catch (IOException e) {
            throw new DBException("Error while loading the EmployeeView.fxml file", e);
        }
    }
    public static void goBackToManagerView() throws IOException, DBException {
        try {
            FXMLLoader loaderEmployee = new FXMLLoader(NavigationUtils.class.getResource("/ManagerView.fxml"));
            Parent root = loaderEmployee.load();

            Stage window = new Stage();
            Scene loginEmployeeScene = new Scene(root);
            window.setTitle("Employee Home Screen");
            window.setResizable(false);
            window.setScene(loginEmployeeScene);
            window.show();
        } catch (IOException e) {
            throw new DBException("Error while loading the EmployeeView.fxml file", e);
        }
    }

}

