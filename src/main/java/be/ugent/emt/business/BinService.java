package be.ugent.emt.business;

import be.ugent.emt.business.models.Bin;
import be.ugent.emt.exceptions.DBException;
import be.ugent.emt.db.MySqlBinDAO;

import java.util.ArrayList;
import java.util.List;


public class BinService {

    // saveBin
    public void saveBin(String binID, int binNumber, double currentAmount, double binCapacity, String wasteType) throws DBException {
        Bin bin = new Bin(binID, binNumber, currentAmount, binCapacity, wasteType);
        MySqlBinDAO.saveBin(bin);
    }

    // updateBin
    public void updateBin(String binID, int binNumber, double currentAmount, double binCapacity, String wasteType) throws DBException {
        Bin bin = new Bin(binID, binNumber, currentAmount, binCapacity, wasteType);
        MySqlBinDAO.updateBin(bin);
    }
    /*
     TODO: implement addBin in MySqlBinDAO if needed
     // addBin
    public void addBin(String binID, int binNumber, double binCapacity, String wasteType) throws DBException {
        Bin bin = new Bin(binID, binNumber, binCapacity, wasteType);
        MySqlBinDAO.addBin(bin);
    }
    */

    // deleteBin
    public void deleteBin(String binID) throws DBException {
        MySqlBinDAO.deleteBin(binID);
    }

    // getBinByID
    public Bin getBinByID(String binID) throws DBException {
        return MySqlBinDAO.getBinByID(binID);
    }

    // getAllBins
    public List<Bin> getAllBins() throws DBException {
        return MySqlBinDAO.getAllBins();

    }

    // getAllPMD
    public List<Bin> getAllPMD() throws DBException {
        return MySqlBinDAO.getAllPMD();
    }

    // getAllRest
    public List<Bin> getAllRest() throws DBException {
        return MySqlBinDAO.getAllRest();
    }

    // getAllGlass
    public List<Bin> getAllGlass() throws DBException {
        return MySqlBinDAO.getAllGlass();
    }

    // getAllPaper
    public List<Bin> getAllPaper() throws DBException {
        return MySqlBinDAO.getAllPaper();
    }

    // getAllBinsByWasteType
    public List<List<Bin>> getAllBinsByWasteType() throws DBException {
        List<Bin> allBins = MySqlBinDAO.getAllBins();
        List<Bin> pmdBins = new ArrayList<>();
        List<Bin> restBins = new ArrayList<>();
        List<Bin> paperBins = new ArrayList<>();
        List<Bin> glassBins = new ArrayList<>();

        for (Bin bin : allBins) {
            switch (bin.getWasteType()) {
                case "PMD" -> pmdBins.add(bin);
                case "REST" -> restBins.add(bin);
                case "PAPER" -> paperBins.add(bin);
                case "GLASS" -> glassBins.add(bin);
            }

        }
        List<List<Bin>> allBinsByWasteType = new ArrayList<>();
        allBinsByWasteType.add(pmdBins);
        allBinsByWasteType.add(restBins);
        allBinsByWasteType.add(paperBins);
        allBinsByWasteType.add(glassBins);
        return allBinsByWasteType;

    }

    // getBinNumberByBinID
    public int getBinNumberByBinID(String binID) throws DBException {
        return MySqlBinDAO.getBinNumberByBinID(binID);
    }

    // updateCurrentBinAmount
    public void updateCurrentBinAmount(String binID, double currentAmount) throws DBException {
        MySqlBinDAO.updateCurrentBinAmount(binID, currentAmount);
    }

    // getCapacityByBinId
    public double getCapacityByBinId(String binID) throws DBException {
        return MySqlBinDAO.getCapacityByBinID(binID);
    }

    // getNumberByBinId
    public int getNumberByBinId(String binID) throws DBException {
        return MySqlBinDAO.getBinNumberByBinID(binID);
    }

    // generateBinId
    public String generateBinId(String wasteType) throws DBException {
        List<Bin> allExistingBins = getAllBins();
        int count = 0;

        for (Bin bin : allExistingBins) {
            if (bin.getWasteType().equals(wasteType)) {
                count++;
            }
        }
        String newBinID = wasteType + "-" + count;
        while (binIDExists(newBinID, allExistingBins)) {
            count++;
            newBinID = wasteType + "-" + count;
        }
        return newBinID;
    }

    private static boolean binIDExists(String binID, List<Bin> existingBins) {
        for (Bin bin : existingBins) {
            if (bin.getBinID().equals(binID)) {
                return true;
            }
        }
        return false;
    }

    public void updateCurrentAmount(String givenBinID, int newCurrentAmountInt) {
    }
}
