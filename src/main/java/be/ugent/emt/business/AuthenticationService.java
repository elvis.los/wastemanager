package be.ugent.emt.business;

import be.ugent.emt.db.MySqlUserDAO;
import be.ugent.emt.business.models.User;
import be.ugent.emt.exceptions.DBException;

public class AuthenticationService {

    // Returns true if username and password are correct
    public boolean isCorrectUsernameAndPwd(String username, String password) throws DBException {
        User user = MySqlUserDAO.getUserByUsername(username);
        if (user != null) {
            return password.equals(user.getPassword());
        }
        return false;
    }


}
