package be.ugent.emt.business;

import be.ugent.emt.business.models.Goal;
import be.ugent.emt.db.MySqlGoalDAO;
import be.ugent.emt.exceptions.DBException;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class GoalService {


    public void addGoal(Goal goal) throws DBException, SQLException {
        int goalID = generateGoalID();
        MySqlGoalDAO.addGoal(goal, goalID);
    }

    public void updateGoal(Goal goal, String[] strings) throws DBException {
        MySqlGoalDAO.update(goal, strings);
    }

    public List<Goal> getGoals() throws DBException {
        return MySqlGoalDAO.getAll();
    }

    public void deleteGoal(int goalID) throws DBException {
        MySqlGoalDAO.delete(goalID);
    }

    public List<List<Goal>> getAllGoalsPerWasteType() throws DBException {
        List<Goal> goals = MySqlGoalDAO.getAll();

        List<Goal> pmdGoals = new ArrayList<>();
        List<Goal> restGoals = new ArrayList<>();
        List<Goal> paperGoals = new ArrayList<>();
        List<Goal> glassGoals = new ArrayList<>();

        for (Goal goal : goals) {
            switch (goal.getWasteType()) {
                case "PMD" -> pmdGoals.add(goal);
                case "REST" -> restGoals.add(goal);
                case "PAPER" -> paperGoals.add(goal);
                case "GLASS" -> glassGoals.add(goal);
            }
        }
        return List.of(pmdGoals, restGoals, paperGoals, glassGoals);
    }

    public boolean isLimitReached(String wasteType) throws DBException {
        List<Goal> goals = MySqlGoalDAO.getAll();
        for (Goal goal : goals) {
            if (goal.getWasteType().equals(wasteType)) {
                return goal.getCurrentAmountRequests() >= goal.getGoalAmountRequests();
            }
        }
        return false;
    }

    public String getGoalStatusPerWasteType(String wasteType) throws DBException {
        List<Goal> goals = MySqlGoalDAO.getAll();
        for (Goal goal : goals) {
            if (goal.getWasteType().equals(wasteType)) {
                // If the current amount of requests is equal to the goal amount of requests, the goal is reached -> return string "limiet bereikt"
                // if the current amount of requests is less than the goal amount of requests, the goal is not reached-> return string "onder limiet"
                // if the current amount of requests is more than the goal amount of requests, the goal is exceeded -> return string "limiet overschreden"
                if (goal.getCurrentAmountRequests() == goal.getGoalAmountRequests()) {
                    return "limiet bereikt";
                } else if (goal.getCurrentAmountRequests() < goal.getGoalAmountRequests()) {
                    return "onder limiet";
                } else {
                    return "limiet overschreden";
                }
            }
        }
        return null;
    }

    // getCurrentAmountOfRequestsPerWasteType
    public int getCurrentAmountOfRequestsPerWasteType(String wasteType) throws DBException {
        List<Goal> goals = MySqlGoalDAO.getAll();
        for (Goal goal : goals) {
            if (goal.getWasteType().equals(wasteType)) {
                return goal.getCurrentAmountRequests();
            }

        }
        return -1;
    }

    // getGoalAmountOfRequestsPerWasteType
    public int getGoalAmountOfRequestsPerWasteType(String wasteType) throws DBException {
        List<Goal> goals = MySqlGoalDAO.getAll();
        for (Goal goal : goals) {
            if (goal.getWasteType().equals(wasteType)) {
                return goal.getGoalAmountRequests();
            }
        }
        return -1;
    }

    // check the goal status for all waste types in  a given month
    public Map<String, String> getGoalStatusForAllWasteTypesAndMonth(int month) throws DBException {
        List<Goal> goals = MySqlGoalDAO.getAll();
        Map<String, String> goalStatusMap = null;
        for (Goal goal : goals) {
            if (goal.getDate().getMonthValue() == month) {
                goalStatusMap.put(goal.getWasteType(), getGoalStatusPerWasteType(goal.getWasteType()));
            }
        }
        return goalStatusMap;
    }

    // check the goal status for a specific waste type in a given month
    public String getGoalStatusForWasteTypeAndMonth(String wasteType, int month) throws DBException {
        List<Goal> goals = MySqlGoalDAO.getAll();
        for (Goal goal : goals) {
            if (goal.getWasteType().equals(wasteType) && goal.getDate().getMonthValue() == month) {
                return getGoalStatusPerWasteType(goal.getWasteType());
            }
        }
        return null;
    }


    public int generateGoalID() throws DBException {
        List<Goal> goals = MySqlGoalDAO.getAll();
        int goalID = 0;
        for (Goal goal : goals) {
            if (goal.getGoalID() > goalID) {
                goalID = goal.getGoalID();
            }
        }
        return goalID + 1;
    }


}
