package be.ugent.emt.business.models;

import be.ugent.emt.db.MySqlUserDAO;
import be.ugent.emt.exceptions.DBException;

import java.util.List;

public class User {

    private final String userName;
    private final String password;
    private final String firstName;
    private final String lastName;
    private final String userType;
    private final String email;

    private final int userId;


    public User(String userName, String password, String firstName, String lastName, String userType, String email, int userId) {
        this.userName = userName;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.userType = userType;
        this.email = email;
        this.userId = userId;
    }


    public String getUserName() {
        return userName;
    }


    public String getPassword() {
        return password;
    }


    public String getFirstName() {
        return firstName;
    }


    public String getLastName() {
        return lastName;
    }


    public String getUserType() {
        return userType;
    }


    public String getEmail() {
        return email;
    }


    public int getUserId() {
        return userId;
    }


}


