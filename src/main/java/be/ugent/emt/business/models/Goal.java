package be.ugent.emt.business.models;

import be.ugent.emt.exceptions.DBException;
import be.ugent.emt.db.MySqlGoalDAO;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class Goal {
    private String wasteType;
    private int goalAmountRequests;
    private int currentAmountRequests;
    private LocalDate date;
    private final int goalID;

    public Goal(String wasteType, int goalAmountRequests, int currentAmountRequests, LocalDate date, int goalID) {
        this.wasteType = wasteType;
        this.goalAmountRequests = goalAmountRequests;
        this.currentAmountRequests = currentAmountRequests;
        this.date = date;
        this.goalID = goalID;

    }

    public String getWasteType() {
        return wasteType;
    }

    public void setWasteType(String wasteType) {
        this.wasteType = wasteType;
    }

    public int getGoalAmountRequests() {
        return goalAmountRequests;
    }

    public void setGoalAmountRequests(int goalAmountRequests) {
        this.goalAmountRequests = goalAmountRequests;
    }

    public int getCurrentAmountRequests() {
        return currentAmountRequests;
    }

    public void setCurrentAmountRequests(int currentAmountRequests) {
        this.currentAmountRequests = currentAmountRequests;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public int getGoalID() {
        return goalID;
    }

    // TODO: deze methode aanpassen als je de bins anders wilt weergeven
    @Override
    public String toString() {
        return "Goal{" +
                "wasteType='" + wasteType + '\'' +
                ", goalAmountRequests=" + goalAmountRequests +
                ", currentAmountRequests=" + currentAmountRequests +
                ", date=" + date +
                ", goalID='" + goalID + '\'' +
                '}';
    }
}