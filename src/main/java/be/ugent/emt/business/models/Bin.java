package be.ugent.emt.business.models;

import be.ugent.emt.exceptions.DBException;

import java.util.ArrayList;
import java.util.List;

public class Bin {
    private String binID; //wasteType en binNumber, methode wordt later aangemaakt
    private int binNumber;
    private double currentAmount = 0.0;
    private double binCapacity;
    private String wasteType;
    private List<Bin> bins;

    //if you want to create instance without giving pm you should add default constructor
    public Bin() {
    }

    public Bin(String binID, int binNumber, double binCapacity, String wasteType) {
        this.binID = binID;
        setBinID();
        this.binNumber = binNumber;
        this.binCapacity = binCapacity;
        this.wasteType = wasteType;
    }

    public Bin(int binNumber, double binCapacity, String wasteType) {
        this.binNumber = binNumber;
        this.binCapacity = binCapacity;
        this.wasteType = wasteType;
    }

    public Bin(String binID, int binNumber, double currentAmount, double binCapacity, String wasteType) {
        this.binID = binID;
        this.binNumber = binNumber;
        this.currentAmount = currentAmount;
        this.binCapacity = binCapacity;
        this.wasteType = wasteType;
    }

    // Methode om binID in te stellen op basis van wasteType en binNumber
    public String setBinID() {
        return wasteType + binNumber;
    }

    public double getBinCapacity() {
        return binCapacity;
    }

    public void setBinCapacity(double binCapacity) {
        this.binCapacity = binCapacity;
    }

    public void setCurrentAmount(double currentAmount) {
        this.currentAmount = currentAmount;
    }

    public double getCurrentAmount() {
        return currentAmount;
    }

    public String getBinID() {
        return binID;
    }

    public void setBinID(String binID) {
        this.binID = binID;
    }

    public int getBinNumber() {
        return binNumber;
    }

    public void setBinNumber(int binNumber) {
        this.binNumber = binNumber;
    }

    public String getWasteType() {
        return wasteType;
    }

    public void setWasteType(String wasteType) {
        this.wasteType = wasteType;
    }

    /*
        //Voeg huidige amount of afval (aflezen van sensor) toe aan Bin via binID
        public void addCurrentAmount(String binID, int newCurrentAmount) throws DBException {

            String binType = binID.substring(0, binID.indexOf("-") - 1);
            double capacity = getCapacityByBinID();
            int binNumber = getBinNumberByBinID();
            double totalAmount = newCurrentAmount + binNumber * capacity;
            double totalLimit = capacity * switch (binType) {
                case "PMD" -> mySqlBinDAO.getAllPMD().size();
                case "REST" -> mySqlBinDAO.getAllRest().size();
                case "GLASS" -> mySqlBinDAO.getAllGlass().size();
                case "PAPER" -> mySqlBinDAO.getAllPaper().size();
                default -> 0; // Handle the default case appropriately
            };

            if (totalAmount < totalLimit) {
                mySqlBinDAO.updateCurrentAmount(binID, newCurrentAmount);
            } else {
                // Set message based on binType
                // messageLabel.setText("Please request a " + binType + " collection");
            }

        }
    */
    public void changeBinByCurrentAmount(int i, String givenBinID) {
        //TODO
    }


    /**
     * // Methode om liters afval toe te voegen en over te gaan naar de volgende vuilbak indien nodig
     * <p>
     * public void addWastePMD(double currentAmount) {
     * if(wasteType.equals("PMD")){
     * double totalAmount = getCurrentAmount() + currentAmount;
     * <p>
     * if (totalAmount > getBinCapacity()) {
     * // Capaciteit van de huidige vuilbak overschreden
     * System.out.println("Capaciteit overschreden voor vuilbak nummer: " + getBinNumber() +
     * " met wasteType: " + wasteType + ". Overgaan naar de volgende vuilbak.");
     * <p>
     * // Reset het huidige aantal liters naar 0
     * setCurrentAmount(0.0);
     * <p>
     * // Ga naar de volgende vuilbak als het niet de laatste container is
     * if (!isLastContainer(totalBins)) {
     * setBinNumber(getBinNumber() + 1);
     * }
     * } else {
     * // Capaciteit niet overschreden, voeg gewoon de liters toe
     * setCurrentAmount(totalAmount);
     * System.out.println(litersToAdd + " liters afval toegevoegd aan vuilbak nummer: " + getBinNumber() +
     * " met wasteType: " + wasteType);
     * }
     * }
     * <p>
     * // Methode om een ophaalverzoek PMD in te dienen voor de laatste container bij overschrijding van een limiet
     * public void checkAndRequestCollectionPMD(double limit, int PMDcounter) {
     * if (getCurrentAmount() > limit && isLastBinPMD(PMDcounter)) {
     * // Indien de laatste container en het aantal liters overschrijdt het limiet, dien een ophaalverzoek in
     * System.out.println("Ophaalverzoek moet worden ingediend voor PMD. Huidig aantal liters: " + getCurrentAmount());
     * }
     * }
     * <p>
     * <p>
     * <p>
     * // Hulpmethode om te controleren of het de laatste container is van PMD
     * private boolean isLastBinPMD(int PMDcounter) {
     * // Implementeer logica om te bepalen of het de laatste container is op basis van het totale aantal vuilbakken van PMD
     * // Return true als het de laatste container is, anders false
     * // Bijvoorbeeld: Als binNumber gelijk is aan PMDcounter, is het de laatste container
     * return getBinNumber() == PMDcounter;
     * }
     * <p>
     * <p>
     * public boolean existsInDB(Integer binNumber) {
     * BinDAO applianceDAO = new BinDAO();
     * return applianceDAO.existsInDB(binNumber);
     * }
     * <p>
     * <p>
     * // Methode om een overzichtstabel te genereren
     * public static void generateOverviewTable(Bin[] bins, double limit, int totalBins) {
     * System.out.println("-------------------------------------------------------------------------");
     * System.out.printf("| %-8s | %-15s | %-13s | %-8s | %-5s |\n", "BinNumber", "wasteType", "TotalAmount", "Limit", "Status");
     * System.out.println("-------------------------------------------------------------------------");
     * <p>
     * for (Bin measurement : bins) {
     * double totalAmount = measurement.getCurrentAmount();
     * WasteType wasteType = measurement.getWasteType();
     * <p>
     * // Controleer op ophaalverzoek en bepaal de status
     * String status = (totalAmount > limit && measurement.isLastContainer(totalBins)) ? "Ophaalverzoek" : "OK";
     * <p>
     * // Afdrukken van de rij in de tabel
     * System.out.printf("| %-8d | %-15s | %-13.2f | %-8.2f | %-5s |\n",
     * measurement.getBinNumber(), wasteType, totalAmount, limit, status);
     * }
     * <p>
     * System.out.println("-------------------------------------------------------------------------");
     * }
     * <p>
     * public static void main(String[] args) {
     * // Voorbeeldgebruik: aantal vuilbakken invoeren
     * int numberOfBins = 3;  // Dit kan worden ingevoerd door de gebruiker of ergens anders worden opgehaald
     * <p>
     * // Aanmaken van een array van Bin-objecten
     * Bin[] bins = new Bin[numberOfBins];
     * <p>
     * // Initialisatie van vuilbakken met unieke ID's, capaciteiten en afvaltypes
     * for (int i = 0; i < numberOfBins; i++) {
     * bins[i] = new Bin("PMD1", i + 1, 0.0, 100.0, WasteType.PMD);
     * }
     * <p>
     * // Voorbeeldgebruik: afval toevoegen aan vuilbakken en controleren op ophaalverzoek
     * bins[0].addWaste(50.0, WasteType.PMD, numberOfBins);
     * bins[1].addWaste(30.0, WasteType.PAPER, numberOfBins);
     * bins[2].addWaste(80.0, WasteType.GLASS, numberOfBins);
     * <p>
     * // Voorbeeldgebruik: controleren op ophaalverzoek
     * for (Bin measurement : bins) {
     * measurement.checkAndRequestCollection(70.0, numberOfBins);
     * }
     * <p>
     * // Nieuwe methode gebruiken om een overzichtstabel te genereren
     * generateOverviewTable(bins, 70.0, numberOfBins);
     * }
     **/

    // TODO: deze methode aanpassen als je de bins anders wilt weergeven
    @Override
    public String toString() {
        return "Bin with ID: " + binID +
                ", number: " + binNumber +
                ", has a current amount of: " + currentAmount +
                ", a capacity of: " + binCapacity + ".";
    }

}


