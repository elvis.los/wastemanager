package be.ugent.emt.business.models;

import java.time.LocalDate;

public class CollectionRequest {
    private int requestID;
    private final String wasteType;
    private final LocalDate date;

    public CollectionRequest(int requestID, String wasteType, LocalDate date) {
        this.requestID = requestID;
        this.wasteType = wasteType;
        this.date = date;
    }

    public String getWasteType() {
        return wasteType;
    }

    public LocalDate getDate() {
        return date;
    }

    public int getRequestID() {
        return requestID;
    }

    public void setRequestID(int requestID) {
        this.requestID = requestID;
    }

}




