package be.ugent.emt.business;

import be.ugent.emt.business.models.CollectionRequest;
import be.ugent.emt.db.MySqlCollectionsDAO;
import be.ugent.emt.exceptions.DBException;

import java.sql.SQLException;
import java.util.List;

public class RequestService {

    // addRequest
    public void addRequest(CollectionRequest collectionRequest) throws DBException, SQLException {
        int requestID = generateRequestID();
        MySqlCollectionsDAO.addRequest(collectionRequest, requestID);
    }

    // updateRequest
    public void updateRequest(CollectionRequest collectionRequest) throws DBException, SQLException {
        MySqlCollectionsDAO.update(collectionRequest);
    }

    // deleteRequest
    public void deleteRequest(int requestID) throws DBException {
        MySqlCollectionsDAO.delete(requestID);
    }

    // getAllRequests
    public List<CollectionRequest> getAllRequests() throws DBException {
        return MySqlCollectionsDAO.getAll();
    }

    // getRequestByWasteType
    public List<CollectionRequest> getRequestByWasteType(String wasteType) throws DBException {
        List<CollectionRequest> requests = MySqlCollectionsDAO.getAll();
        for (CollectionRequest request : requests) {
            if (request.getWasteType().equals(wasteType)) {
                requests.add(request);
            }
        }
        return requests;
    }

    // getTotalAmountOfRequestsPerWasteType
    public List<CollectionRequest> getTotalAmountOfRequestsPerWasteType(String wasteType) throws DBException {
        List<CollectionRequest> requests = MySqlCollectionsDAO.getAll();
        for (CollectionRequest request : requests) {
            if (request.getWasteType().equals(wasteType)) {
                requests.add(request);
            }
        }
        return requests;
    }


    // getRequestByRequestID
    public CollectionRequest getRequestByRequestID(int requestID) throws DBException {
        List<CollectionRequest> requests = MySqlCollectionsDAO.getAll();
        for (CollectionRequest request : requests) {
            if (request.getRequestID() == requestID) {
                return request;
            }
        }
        return null;
    }

    // getAllRequestsByWasteTypeForMonth
    public List<CollectionRequest> getAllRequestsByWasteTypeForMonth(String wasteType, int month) throws DBException {
        List<CollectionRequest> requests = MySqlCollectionsDAO.getAll();
        for (CollectionRequest request : requests) {
            if (request.getWasteType().equals(wasteType) && request.getDate().getMonthValue() == month) {
                requests.add(request);
            }
        }
        return requests;
    }


    // generateRequestID
    public int generateRequestID() throws DBException {
        List<CollectionRequest> requests = MySqlCollectionsDAO.getAll();
        int max = 0;
        for (CollectionRequest request : requests) {
            int requestID = request.getRequestID();
            if (requestID > max) {
                max = requestID;
            }
        }
        return max + 1;
    }

    // countRequestsByWasteType
    public int countRequestsByWasteType(String wasteType) throws DBException {
        List<CollectionRequest> requests = MySqlCollectionsDAO.getAll();
        int count = 0;
        for (CollectionRequest request : requests) {
            if (request.getWasteType().equals(wasteType)) {
                count++;
            }
        }
        return count;
    }


}
