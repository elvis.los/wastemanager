package be.ugent.emt.business;

import be.ugent.emt.business.models.User;
import be.ugent.emt.db.MySqlUserDAO;
import be.ugent.emt.exceptions.DBException;

import java.sql.SQLException;
import java.util.List;

public class UserService {

    // In onze database hebben wij allemaal verschillende soorten userTypes, wij moeten die gewoon Employee en Manager noemen!
    // -> hieronder zet je twee constanten die je gebruikt in de database
    private static final String EMPLOYEE_TYPE_DB = "Employee";
    private static final String MANAGER_TYPE_DB = "Manager";

    public void updateUser(String username, String password, String firstName, String lastName, String userType, String email, int userId) throws DBException {
        User user = new User(username, password, firstName, lastName, userType, email, userId);
        MySqlUserDAO.updateUser(user);
    }

    public void deleteUser(String username) throws DBException {
        MySqlUserDAO.delete(username);
    }

    public List<User> getAllUsers() throws DBException {
        return MySqlUserDAO.getAllUsers();
    }

    public void addUser(String username, String password, String firstName, String lastName, String userType, String email, int userId) throws DBException {
        User user = new User(username, password, firstName, lastName, userType, email, userId);
        MySqlUserDAO.addUser(user);
    }

    public void addEmployee(int userId) throws DBException {
        MySqlUserDAO.addAsEmployee(userId);
    }

    public void addManager(int userId) throws DBException {
        MySqlUserDAO.addAsManager(userId);
    }

    public boolean userExists(String username) throws DBException {
        User user = MySqlUserDAO.getUserByUsername(username);
        return user != null;
    }

    public String getUserType(String username) throws DBException {
        User user = MySqlUserDAO.getUserByUsername(username);
        if (user != null) {
            return user.getUserType();
        }
        return null;
    }

    public boolean isEmployee(String username) throws DBException {
        User user = MySqlUserDAO.getUserByUsername(username);
        if (user != null) {
            return user.getUserType().equals(EMPLOYEE_TYPE_DB);
        }
        return false;
    }

    public boolean isWasteManager(String username) throws DBException {
        User user = MySqlUserDAO.getUserByUsername(username);
        if (user != null) {
            return user.getUserType().equals(MANAGER_TYPE_DB);
        }
        return false;
    }



    public Integer createUserID() throws DBException, SQLException {
        List<User> users = getAllUsers();
        int userID = 0;
        for (User user : users) {
            if (user.getUserId() > userID) {
                userID = user.getUserId();
            }
        }
        return userID + 1;
    }
}
