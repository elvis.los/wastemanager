package be.ugent.emt.db;

import be.ugent.emt.db.util.DBConnector;
import be.ugent.emt.exceptions.DBException;
import be.ugent.emt.business.models.Bin;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MySqlBinDAO {
    private static final String SELECT_BIN_BY_ID = "SELECT * FROM bin WHERE binID=?";
    private static final String INSERT_BIN = "INSERT INTO bin (binID, binNumber, currentAmount, binCapacity, wasteType) VALUES (?, ?, ?, ?, ?)";
    private static final String SELECT_ALL_BINS = "SELECT * FROM bin";
    private static final String UPDATE_BIN = "UPDATE bin SET binNumber=?, binCapacity=?, currentAmount=?, wasteType=? WHERE binID=?";
    private static final String DELETE_BIN = "DELETE FROM bin WHERE binID=?";
    private static final String SELECT_ALL_PMD_BINS = "SELECT * FROM bin WHERE wasteType='PMD'";
    private static final String SELECT_ALL_REST_BINS = "SELECT * FROM bin WHERE wasteType='REST'";
    private static final String SELECT_ALL_GLASS_BINS = "SELECT * FROM bin WHERE wasteType='GLASS'";
    private static final String SELECT_ALL_PAPER_BINS = "SELECT * FROM bin WHERE wasteType='PAPER'";
    private static final String UPDATE_CURRENT_AMOUNT = "UPDATE bin SET currentAmount=? WHERE binID = ?";
    private static final String SELECT_CAPACITY_BY_BIN_ID = "SELECT binCapacity FROM bin WHERE binID = ?";
    private static final String SELECT_BIN_NUMBER_BY_BIN_ID = "SELECT binNumber FROM bin WHERE binID = ?";

    private static Bin createBinFromResultSet(ResultSet resultSet) throws SQLException {
        String binID = resultSet.getString("binID");
        int binNumber = resultSet.getInt("binNumber");
        double currentAmount = resultSet.getDouble("currentAmount");
        double binCapacity = resultSet.getDouble("binCapacity");
        String wasteType = resultSet.getString("wasteType");

        return new Bin(binID, binNumber, currentAmount, binCapacity, wasteType);
    }

    public static Bin getBinByID(String binID) throws DBException {
        try (Connection con = DBConnector.getConnection();
             PreparedStatement statement = con.prepareStatement(SELECT_BIN_BY_ID)) {

            statement.setString(1, binID);
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    return createBinFromResultSet(resultSet);
                }
            }
        } catch (SQLException e) {
            throw new DBException("Error retrieving bin with ID: " + binID, e);
        }
        return null;
    }

    public static void saveBin(Bin bin) throws DBException {
        try (Connection con = DBConnector.getConnection();
             PreparedStatement statement = con.prepareStatement(INSERT_BIN)) {

            statement.setString(1, bin.getBinID());
            statement.setInt(2, bin.getBinNumber());
            statement.setDouble(3, bin.getCurrentAmount());
            statement.setDouble(4, bin.getBinCapacity());
            statement.setString(5, bin.getWasteType());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DBException("Error saving bin", e);
        }
    }

    public static List<Bin> getAllBins() throws DBException {
        List<Bin> bins = new ArrayList<>();
        try (Connection con = DBConnector.getConnection();
             PreparedStatement statement = con.prepareStatement(SELECT_ALL_BINS);
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                bins.add(createBinFromResultSet(resultSet));
            }
        } catch (SQLException e) {
            throw new DBException("Error retrieving all bins", e);
        }
        return bins;
    }

    public static void updateBin(Bin bin) throws DBException {
        try (Connection con = DBConnector.getConnection();
             PreparedStatement statement = con.prepareStatement(UPDATE_BIN)) {

            statement.setInt(1, Integer.parseInt(bin.getBinID()));
            statement.setDouble(2, bin.getBinNumber());
            statement.setDouble(3, bin.getCurrentAmount());
            statement.setDouble(4, bin.getBinCapacity());
            statement.setString(5, bin.getBinID());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DBException("Error updating bin", e);
        }
    }

    public static void deleteBin(String binID) throws DBException {
        try (Connection con = DBConnector.getConnection();
             PreparedStatement statement = con.prepareStatement(DELETE_BIN)) {

            statement.setString(1, binID);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DBException("Error deleting bin with ID: " + binID, e);
        }
    }

    public static List<Bin> getAllPMD() throws DBException {
        List<Bin> pmdBins = new ArrayList<>();
        try (Connection con = DBConnector.getConnection();
             PreparedStatement statement = con.prepareStatement(SELECT_ALL_PMD_BINS);
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                pmdBins.add(createBinFromResultSet(resultSet));
            }
        } catch (SQLException e) {
            throw new DBException("Error retrieving all PMD bins", e);
        }
        return pmdBins;
    }

    public static List<Bin> getAllRest() throws DBException {
        List<Bin> restBins = new ArrayList<>();
        try (Connection con = DBConnector.getConnection();
             PreparedStatement statement = con.prepareStatement(SELECT_ALL_REST_BINS);
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                restBins.add(createBinFromResultSet(resultSet));
            }
        } catch (SQLException e) {
            throw new DBException("Error retrieving all REST bins", e);
        }
        return restBins;
    }

    public static List<Bin> getAllGlass() throws DBException {
        List<Bin> glassBins = new ArrayList<>();
        try (Connection con = DBConnector.getConnection();
             PreparedStatement statement = con.prepareStatement(SELECT_ALL_GLASS_BINS);
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                glassBins.add(createBinFromResultSet(resultSet));
            }
        } catch (SQLException e) {
            throw new DBException("Error retrieving all GLASS bins", e);
        }
        return glassBins;
    }

    public static List<Bin> getAllPaper() throws DBException {
        List<Bin> paperBins = new ArrayList<>();
        try (Connection con = DBConnector.getConnection();
             PreparedStatement statement = con.prepareStatement(SELECT_ALL_PAPER_BINS);
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                paperBins.add(createBinFromResultSet(resultSet));
            }
        } catch (SQLException e) {
            throw new DBException("Error retrieving all PAPER bins", e);
        }
        return paperBins;
    }

    public static void updateCurrentBinAmount(String binID, double currentAmount) throws DBException {
        try (Connection con = DBConnector.getConnection();
             PreparedStatement statement = con.prepareStatement(UPDATE_CURRENT_AMOUNT)) {

            statement.setDouble(1, currentAmount);
            statement.setString(2, binID);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DBException("Error updating current amount for bin with ID: " + binID, e);
        }
    }

    public static double getCapacityByBinID(String binID) throws DBException {
        try (Connection con = DBConnector.getConnection();
             PreparedStatement statement = con.prepareStatement(SELECT_CAPACITY_BY_BIN_ID)) {

            statement.setString(1, binID);
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    return resultSet.getDouble("binCapacity");
                } else {
                    throw new DBException("No capacity found for bin with ID: " + binID);
                }
            }
        } catch (SQLException e) {
            throw new DBException("Error retrieving capacity for bin with ID: " + binID, e);
        }
    }

    public static int getBinNumberByBinID(String binID) throws DBException {
        try (Connection con = DBConnector.getConnection();
             PreparedStatement statement = con.prepareStatement(SELECT_BIN_NUMBER_BY_BIN_ID)) {

            statement.setString(1, binID);
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    return resultSet.getInt("binNumber");
                } else {
                    throw new DBException("No bin number found for bin with ID: " + binID);
                }
            }
        } catch (SQLException e) {
            throw new DBException("Error retrieving bin number for bin with ID: " + binID, e);
        }
    }


}
