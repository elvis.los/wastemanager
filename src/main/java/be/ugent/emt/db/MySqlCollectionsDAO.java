package be.ugent.emt.db;

import be.ugent.emt.db.util.DAOInterface;
import be.ugent.emt.db.util.DBConnector;
import be.ugent.emt.exceptions.DBException;
import be.ugent.emt.business.models.CollectionRequest;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MySqlCollectionsDAO implements DAOInterface<CollectionRequest> {
    private static final String INSERT_INTO_COLLECTIONREQUESTS = "INSERT INTO collectionrequests (requestID, wasteType, date) VALUES (?, ?, ?)";
    private static final String SELECT_BY_REQUESTID = "SELECT * FROM collectionrequests WHERE requestID=?";
    private static final String SELECT_ALL_COLLECTIONREQUESTS = "SELECT * FROM collectionrequests";
    private static final String DELETE_COLLECTIONREQUEST = "DELETE FROM collectionrequests WHERE requestID=?";
    private static final String SELECT_COUNT_BY_WASTETYPE = "SELECT wasteType, COUNT(*) FROM collectionrequests GROUP BY wasteType";
    private static final String SELECT_COUNT_BY_WASTETYPE_AND_MONTH = "SELECT wasteType, MONTH(date) FROM collectionrequests";
    private static final String UPDATE_COLLECTIONREQUEST = "UPDATE collectionrequests SET wasteType=?, date=? WHERE requestID=?";


    // Voegt collectionrequest object toe aan database
    public static void addRequest(CollectionRequest collectionRequest, int requestID) throws SQLException, DBException {
        try (
                Connection con = DBConnector.getConnection();
                PreparedStatement insertStatement = con.prepareStatement(INSERT_INTO_COLLECTIONREQUESTS)
        ) {

            Date currentDate = Date.valueOf(collectionRequest.getDate());
            insertStatement.setInt(1, requestID);
            insertStatement.setString(2, collectionRequest.getWasteType());
            insertStatement.setDate(3, currentDate);
            insertStatement.executeUpdate();

        } catch (SQLException e) {
            throw new DBException("We konden de aanvraag niet opslaan in de database", e);
        }
    }


    public static CollectionRequest getRequestByRequestID(int requestID) throws DBException {
        String wasteType;
        LocalDate date;

        try (
                Connection con = DBConnector.getConnection();
                PreparedStatement statement = con.prepareStatement(SELECT_BY_REQUESTID)
        ) {

            statement.setInt(1, requestID);
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next() && resultSet.getString(1) != null) {

                    // Extract data from result set and create a new CollectionRequest object
                    wasteType = resultSet.getString("wasteType");
                    date = resultSet.getDate("date").toLocalDate();

                    // Initialize and return a new CollectionRequest object
                    return new CollectionRequest(requestID, wasteType, date);
                }
            }
        } catch (SQLException e) {
            throw new DBException("Error retrieving collection request by requestID", e);
        }
        return null; // Or consider throwing an exception if the request is not found
    }


    public static List<CollectionRequest> getAll() throws DBException {
        List<CollectionRequest> collectionRequests = new ArrayList<>();

        try (Connection con = DBConnector.getConnection();
             PreparedStatement statement = con.prepareStatement(SELECT_ALL_COLLECTIONREQUESTS);
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                int requestID = resultSet.getInt("requestID");
                String wasteType = resultSet.getString("wasteType");
                LocalDate date = resultSet.getDate("date").toLocalDate();
                collectionRequests.add(new CollectionRequest(requestID, wasteType, date));
            }
        } catch (SQLException e) {
            throw new DBException("Error retrieving all collection requests", e);
        }
        return collectionRequests;
    }


    public static void delete(int requestID) throws DBException {
        try (Connection con = DBConnector.getConnection();
             PreparedStatement statement = con.prepareStatement(DELETE_COLLECTIONREQUEST)) {

            statement.setInt(1, requestID);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DBException("Error deleting collection request", e);
        }
    }


    public Map<String, Integer> getCountRequestsByWasteType() throws DBException {
        Map<String, Integer> counts = new HashMap<>();
        try (Connection con = DBConnector.getConnection();
             PreparedStatement statement = con.prepareStatement(SELECT_COUNT_BY_WASTETYPE);
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                String wasteType = resultSet.getString("wasteType");
                int requestCount = resultSet.getInt(2);
                counts.put(wasteType, requestCount);
            }
        } catch (SQLException e) {
            throw new DBException("Error getting count of requests by waste type", e);
        }
        return counts;
    }


    public Map<String, Integer> getCountRequestsByWasteTypeAndMonth() throws DBException {
        Map<String, Integer> counts = new HashMap<>();

        try (Connection con = DBConnector.getConnection();
             PreparedStatement statement = con.prepareStatement(SELECT_COUNT_BY_WASTETYPE_AND_MONTH);
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                String wasteType = resultSet.getString("wasteType");
                int month = resultSet.getInt(2);
                String key = wasteType + "-" + month;
                counts.put(key, counts.getOrDefault(key, 0) + 1);
            }
        } catch (SQLException e) {
            throw new DBException("Error getting count of requests by waste type and month", e);
        }
        return counts;
    }


    public static void update(CollectionRequest collectionRequest) throws DBException, SQLException {
        try (Connection con = DBConnector.getConnection();
             PreparedStatement statement = con.prepareStatement(UPDATE_COLLECTIONREQUEST)) {

            statement.setString(1, collectionRequest.getWasteType());
            statement.setDate(2, Date.valueOf(collectionRequest.getDate()));
            statement.setInt(3, collectionRequest.getRequestID());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DBException("Error updating collection request", e);
        }
    }
}