package be.ugent.emt.db;

import be.ugent.emt.db.util.DBConnector;
import be.ugent.emt.exceptions.DBException;
import be.ugent.emt.business.models.Goal;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class MySqlGoalDAO {
    private static final String INSERT_GOAL = "INSERT INTO goal (wasteType, goalAmountRequests, date, goalID) VALUES (?, ?, ?, ?)";
    private static final String SELECT_ALL_GOALS = "SELECT * FROM goal";
    private static final String UPDATE_GOAL = "UPDATE goal SET wasteType=?, goalAmountRequests=?, date=? WHERE goalID=?";
    private static final String DELETE_GOAL = "DELETE FROM goal WHERE goalID=?";

    public static void addGoal(Goal goal, int goalID) throws DBException {
        try (Connection con = DBConnector.getConnection();
             PreparedStatement statement = con.prepareStatement(INSERT_GOAL)) {

            statement.setString(1, goal.getWasteType());
            statement.setInt(2, goal.getGoalAmountRequests());
            statement.setInt(3, goal.getCurrentAmountRequests());
            statement.setDate(4, Date.valueOf(goal.getDate()));
            statement.setInt(5, goalID);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DBException("Error saving goal", e);
        }
    }

    public static List<Goal> getAll() throws DBException {
        List<Goal> goals = new ArrayList<>();
        try (Connection con = DBConnector.getConnection();
             PreparedStatement statement = con.prepareStatement(SELECT_ALL_GOALS);
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                goals.add(createGoalFromResultSet(resultSet));
            }
            return goals;
        } catch (SQLException e) {
            throw new DBException("Error retrieving all goals", e);
        }
    }

    public static void update(Goal goal, String[] parameters) throws DBException {
        try (Connection con = DBConnector.getConnection();
             PreparedStatement statement = con.prepareStatement(UPDATE_GOAL)) {

            statement.setString(1, parameters[0]);
            statement.setInt(2, Integer.parseInt(parameters[1]));
            statement.setInt(3, Integer.parseInt(parameters[2]));
            statement.setDate(4, Date.valueOf(parameters[2]));
            statement.setInt(5, goal.getGoalID());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DBException("Error updating goal", e);
        }
    }

    public static void delete(int goalID) throws DBException {
        try (Connection con = DBConnector.getConnection();
             PreparedStatement statement = con.prepareStatement(DELETE_GOAL)) {

            statement.setInt(1, goalID);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DBException("Error deleting goal", e);
        }
    }

    private static Goal createGoalFromResultSet(ResultSet resultSet) throws SQLException {
        String wasteType = resultSet.getString("wasteType");
        int goalAmountRequests = resultSet.getInt("goalAmountRequests");
        int currentAmountRequests = resultSet.getInt("currentAmountRequests");
        LocalDate date = resultSet.getDate("date").toLocalDate();
        int goalID = resultSet.getInt("goalID");

        return new Goal(wasteType, goalAmountRequests, currentAmountRequests, date, goalID);
    }
}
