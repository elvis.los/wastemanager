package be.ugent.emt.db;


import be.ugent.emt.db.util.DAOInterface;
import be.ugent.emt.db.util.DBConnector;
import be.ugent.emt.exceptions.DBException;
import be.ugent.emt.business.models.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MySqlUserDAO implements DAOInterface<User> {
    private static final String INSERT_USER = "INSERT INTO user (userName, password, firstName, lastName, userType, email, userID) VALUES (?, ?, ?, ?, ?, ?, ?)";
    private static final String SELECT_ALL_USERS = "SELECT * FROM user";
    private static final String SELECT_USER_BY_USERNAME = "SELECT * FROM user WHERE userName=?";
    private static final String UPDATE_USER = "UPDATE user SET password=?, firstName=?, lastName=?, userType=?, email=?, userID=? WHERE userName=?";
    private static final String DELETE_USER = "DELETE FROM user WHERE userName=?";
    private static final String GET_USER_BY_USERID = "SELECT * FROM user WHERE userID=?";

    private static final String INSERT_EMPLOYEE = "INSERT INTO employee (userID) VALUES (?)";
    private static final String INSERT_MANAGER = "INSERT INTO wastemanager (userID) VALUES (?)";

    private static User createUserFromResultSet(ResultSet resultSet) throws SQLException {
        String userName = resultSet.getString("userName");
        String password = resultSet.getString("password");
        String firstName = resultSet.getString("firstName");
        String lastName = resultSet.getString("lastName");
        String userType = resultSet.getString("userType");
        String email = resultSet.getString("email");
        int userID = resultSet.getInt("userID");

        return new User(userName, password, firstName, lastName, userType, email, userID);
    }

    public static void addUser(User user) throws DBException {
        try (Connection con = DBConnector.getConnection();
             PreparedStatement statement = con.prepareStatement(INSERT_USER)) {

            statement.setString(1, user.getUserName());
            statement.setString(2, user.getPassword());
            statement.setString(3, user.getFirstName());
            statement.setString(4, user.getLastName());
            statement.setString(5, user.getUserType());
            statement.setString(6, user.getEmail());
            statement.setInt(7, user.getUserId());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DBException("Error saving user", e);
        }
    }


    public static List<User> getAllUsers() throws DBException {
        List<User> users = new ArrayList<>();
        try (Connection con = DBConnector.getConnection();
             PreparedStatement statement = con.prepareStatement(SELECT_ALL_USERS);
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                users.add(createUserFromResultSet(resultSet));
            }
            return users;
        } catch (SQLException e) {
            throw new DBException("Error retrieving all users", e);
        }
    }


    public static User getUserByUsername(String userName) throws DBException {
        try (Connection con = DBConnector.getConnection();
             PreparedStatement statement = con.prepareStatement(SELECT_USER_BY_USERNAME)) {

            statement.setString(1, userName);
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    return createUserFromResultSet(resultSet);
                }
                return null;
            }
        } catch (SQLException e) {
            throw new DBException("Error retrieving user by username", e);
        }
    }


    public static void updateUser(User user) throws DBException {
        try (Connection con = DBConnector.getConnection();
             PreparedStatement statement = con.prepareStatement(UPDATE_USER)) {

            statement.setString(1, user.getPassword());
            statement.setString(2, user.getFirstName());
            statement.setString(3, user.getLastName());
            statement.setString(4, user.getUserType());
            statement.setString(5, user.getEmail());
            statement.setInt(6, user.getUserId());
            statement.setString(7, user.getUserName());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DBException("Error updating user", e);
        }

    }

    public static void delete(String userName) throws DBException {
        try (Connection con = DBConnector.getConnection();
             PreparedStatement statement = con.prepareStatement(DELETE_USER)) {

            statement.setString(1, userName);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DBException("Error deleting user", e);
        }
    }

    public static User getUserByUserID(String userID) throws DBException, SQLException {
        try (Connection con = DBConnector.getConnection();
             PreparedStatement statement = con.prepareStatement(GET_USER_BY_USERID)) {

            statement.setString(1, userID);
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    return createUserFromResultSet(resultSet);
                }
                return null;
            } catch (SQLException e) {
                throw new DBException("Error retrieving user by userID", e);
            }

        }


    }

    public static void addAsEmployee(int userID) throws DBException {
        try (Connection con = DBConnector.getConnection();
             PreparedStatement statement = con.prepareStatement(INSERT_EMPLOYEE)) {
            statement.setInt(1, (userID));
            statement.executeUpdate();

        } catch (SQLException e) {
            throw new DBException("Error saving employee", e);
        }

    }

    public static void addAsManager(int userId) throws DBException {
        try (Connection con = DBConnector.getConnection();
             PreparedStatement statement = con.prepareStatement(INSERT_MANAGER)) {
            statement.setInt(1, userId);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DBException("Error saving manager", e);
        }
    }
}



