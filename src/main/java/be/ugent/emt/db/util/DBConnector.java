//necessary imports
package be.ugent.emt.db.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnector {

    private static Connection con;
    private static final String DBName = "db2023_08";
    private static final String DBPassword = "652fd3c17a578";
    private static final String jdbcURL = "jdbc:mysql://pdbmbook.com:3306/db2023_08";


    private DBConnector() {
    }

    public static Connection getConnection() throws SQLException {
        try {
            DriverManager.getConnection(jdbcURL, DBName, DBPassword);
        } catch (SQLException e) {
            throw new SQLException("Error while connecting to database", e);
        }
        return DriverManager.getConnection(jdbcURL, DBName, DBPassword);
    }
}




